# ''' O_|_| | _|_| | O_| | _|_| '''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import itertools
from sklearn.model_selection import cross_val_score , train_test_split , learning_curve , validation_curve , GridSearchCV , KFold , RepeatedKFold
from sklearn.feature_selection import VarianceThreshold , SelectFromModel , RFE
from sklearn.ensemble import ExtraTreesClassifier ,RandomForestClassifier , VotingClassifier
from sklearn.preprocessing import LabelEncoder , PolynomialFeatures , MinMaxScaler , StandardScaler
from sklearn.linear_model import LinearRegression , LogisticRegression , SGDClassifier , Lasso , Ridge
from sklearn.dummy import DummyRegressor , DummyClassifier
from sklearn.metrics import accuracy_score , precision_score , recall_score, f1_score , classification_report , confusion_matrix , mean_squared_error, r2_score , roc_auc_score , roc_curve, auc
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import  GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import Pipeline , make_pipeline
from sklearn.svm import SVC, LinearSVC
from sklearn.tree import DecisionTreeClassifier
import warnings
warnings.filterwarnings('ignore')
warnings.filterwarnings(action='ignore',category=DeprecationWarning)
warnings.filterwarnings(action='ignore',category=FutureWarning)



from time import sleep
import matplotlib as mpl
import matplotlib.animation
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.style as style
from matplotlib import lines
from matplotlib.colors import ListedColormap
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pandas as pd
import plotly.express as px
import seaborn as sns
from sklearn.cluster import estimate_bandwidth
from sklearn.cluster import KMeans
from sklearn.cluster import MeanShift
from sklearn.datasets import make_blobs
from sklearn.datasets import make_moons




def PolynomialRegression(degree=2, **kwargs):
    return make_pipeline(PolynomialFeatures(degree),
                         LinearRegression(**kwargs))

def feature_selection(select, X_train, y_train,visual=False):
    select.fit(X_train, y_train)
    X_train_select = select.transform(X_train)
    print("X_train.shape: {}".format(X_train.shape))
    print("X_train_select.shape: {}".format(X_train_select.shape))
    mask = select.get_support()

    # visualize the mask. black is True, white is False
    plt.matshow(mask.reshape(1, -1), cmap='gray_r')
    plt.xlabel("Sample index")
    plt.yticks(())
    plt.show()


    if(visual):
        importances = mask
        # std = np.std([tree.get_support() for tree in select.estimators_],
        #              axis=0)
        indices = np.argsort(importances)[::-1]
        fnames = [feature_cols[i] for i in indices]

        plt.figure(figsize=(10, 10))
        plt.title("Feature importances")
        plt.bar(range(X.shape[1]), importances[indices],
                color="r", align="center")
        plt.xticks(range(X.shape[1]), fnames, rotation=90)
        plt.xlim([-1, X.shape[1]])
        plt.show()




    # test model after select feature
    X_test_select = select.transform(X_test)

    score = logreg.fit(X_train_select, y_train).score(X_test_select, y_test)
    print("Test score: {:.3f}".format(score))

def feature_selection2(select, X_train, y_train,visual=False):
    select.fit(X_train, y_train)
    mask = select.feature_importances_

    # visualize the mask. black is True, white is False
    plt.matshow(mask.reshape(1, -1), cmap='gray_r')
    plt.xlabel("Sample index")
    plt.yticks(())
    plt.show()


    if(visual):
        std = np.std([tree.feature_importances_ for tree in select.estimators_],
                     axis=0)
        indices = np.argsort(mask)[::-1]
        fnames = [feature_cols[i] for i in indices]

        # Plot the feature importances of the select
        plt.figure(figsize=(10, 10))
        plt.title("Feature importances")
        plt.bar(range(X.shape[1]), mask[indices],
                color="r", yerr=std[indices], align="center")
        plt.xticks(range(X.shape[1]), fnames, rotation=90)
        plt.xlim([-1, X.shape[1]])
        plt.show()

def voting_fet_target(target_col,feature_cols,data):
    X = data[feature_cols].fillna(0)
    y = data[target_col].fillna(0)

    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)

    log_clf = LogisticRegression(random_state=42)
    dt_clf = DecisionTreeClassifier(random_state=42)
    nb_clf = GaussianNB()

    voting_clf = VotingClassifier(
        estimators=[('lr', log_clf), ('rf', dt_clf), ('nb', nb_clf)],
        voting='soft')

    # 'hard'

    voting_clf.fit(X_train, y_train)
    for clf in (log_clf, dt_clf, nb_clf, voting_clf):
        clf.fit(X_train, y_train)
        model_cm_score(clf, X_test, y_test, X_train, y_train)

def model_cm_score(model,X_test,y_test,X_train, y_train):
    print("---------------------------------------------------------")
    print(model.__str__(),'\n')
    print('Train Accuracy: {:.2f}'.format(model.score(X_train, y_train)))
    print('Test Accuracy: {:.2f}'.format(model.score(X_test, y_test)))

    model_predicted = model.predict(X_test)
    print('Accuracy: {:.2f}'.format(accuracy_score(y_test, model_predicted)))

    # print('Precision: {:.2f}'.format(precision_score(y_test, model_predicted)))
    # print('Recall: {:.2f}'.format(recall_score(y_test, model_predicted)))
    # print('F1: {:.2f}'.format(f1_score(y_test, model_predicted)))

def train_and_test(classifier,target_col,feature_cols,data):
    X = data[feature_cols].fillna(0)
    y = data[target_col].fillna(0)
    print(list(data.columns))
    print(X.shape)
    print(y.shape)

    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
    classifier.fit(X_train, y_train)
    y_pred = classifier.predict(X_test)
    y_score = classifier.predict_proba(X_test)[:, 1]
    # y_score = classifier.decision_function(X_test)[:, 1]
    accuracy = classifier.score(X_test, y_test)

    # print("Accuracy: {}%".format(int(round(accuracy * 100))))
    accuracy = int(round(accuracy * 100))
    con_matrix = confusion_matrix(y_test, y_pred)
    # print(confusion_matrix(y_test, y_pred))
    classification_rep = classification_report(y_test, y_pred)
    # print(classification_report(y_test, y_pred))
    fpr, tpr, _ = roc_curve(y_test, y_score)
    roc_auc = auc(fpr, tpr)


    # ploting confustion matrix
    plt.figure(figsize=(12, 12))
    plt.subplot(2, 1, 1)
    sns.heatmap(con_matrix, annot=True, fmt="d")
                # xticklabels=target_names, yticklabels=target_names)
    plt.ylabel("Real value")
    plt.xlabel("Predicted value")
    plt.show()

    # print scores
    model_cm_score(classifier, X_test, y_test, X_train, y_train)
    print("accuracy  score: {} %".format(accuracy))
    print("auc  score: {} ".format(roc_auc))
    print(classification_rep)

    # print ROC curve
    plt.figure()
    plt.plot(fpr, tpr, color='darkorange',
             lw=2, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()


# def select_fet_target(model,target_col,feature_cols,data):
#     X = data[feature_cols].fillna(0)
#     y = data[target_col].fillna(0)
#     print(list(data.columns))
#     print(X.shape)
#     print(y.shape)
#
#     X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
#
#     model.fit(X_train, y_train)
#     model_cm_score(model, X_test, y_test,X_train, y_train)





data =pd.read_csv('data/gtd.csv',encoding='ISO-8859-1')


data_columns = [
    # 'eventid',
    # ===== Spatio-Temporal Variables =====
    'iyear', 'imonth', 'iday', 'latitude', 'longitude',

    # ===== Binary Variables (1 -> yes or 0 -> no) =====
    'extended',
    'vicinity',
    'crit1', 'crit2', 'crit3',
    'doubtterr',
    'multiple',
    'success',
    'suicide',
    'claimed',
    'property',
    'ishostkid',

    # ===== Continuous Variables =====
    'nkill',
    'nwound',

    # ===== Categorical variables =====
    'country_txt',
    'region',
    'region_txt',
    'attacktype1_txt',
    'targtype1_txt',
    'natlty1_txt',
    'weaptype1_txt',
'weapsubtype1_txt',

    # ===== Descriptive Variables =====
    'target1',
    'gname',
    'summary',
]



# 2.3. Cleaning the data

data = data.loc[:, data_columns]
data = data[(data.crit1 == 1) & (data.crit2 == 1) & (data.crit3 == 1) & (data.doubtterr == 0)]

data.weaptype1_txt.replace('Vehicle (not to include vehicle-borne explosives, i.e., car or truck bombs)','Vehicle', inplace = True)
# -9 -> 0
data.iloc[:,[6, 15, 16, 17]] = data.iloc[:,[6, 15, 16, 17]].replace(-9,0)
# 2-> 1
data.claimed.replace(2,1, inplace = True)


# lower
data.target1 = data.target1.str.lower()
data.gname = data.gname.str.lower()
data.summary = data.summary.str.lower()
data.target1 = data.target1.fillna('unknown').replace('unk','unknown')
# median

# استبدال بعض القيم الغير موجودة بوسطي قيم هذا العمود
data.nkill = np.round(data.nkill.fillna(data.nkill.median())).astype(int)
data.nwound = np.round(data.nwound.fillna(data.nwound.median())).astype(int)

data['ncasualties'] = data['nkill'] + data['nwound']
data['has_casualties'] = data['ncasualties'].apply(lambda x: 0 if x == 0 else 1)

print("Data cleaned and prepared.")





''' terrorist_weather_jan2012_dec_2016 '''

data_w = pd.read_csv('data/terrorist_weather_jan2012_dec_2016.csv', encoding='ISO-8859-1')

# lb = LabelEncoder()
#
# data_w['country_txt'] = lb.fit_transform(data_w['country_txt'])
# data_w['region_txt'] = lb.fit_transform(data_w['region_txt'])
# data_w['attacktype1_txt'] = lb.fit_transform(data_w['attacktype1_txt'])
# data_w['targtype1_txt'] = lb.fit_transform(data_w['targtype1_txt'])
# data_w['weaptype1_txt'] = lb.fit_transform(data_w['weaptype1_txt'])
#
# feature_cols = [
#     't2m', 'tcc', 'vidgf', 'sp', 'v10',
#  # ===== Spatio-Temporal Variables =====
#             'iyear', 'imonth', 'iday',
#             'latitude', 'longitude',
#             # ===== Binary Variables (1 -> yes or 0 -> no) =====
#             'extended',
#             'vicinity',
#             'doubtterr',
#             'multiple',
#             # 'success',
#             'suicide',
#             'claimed',
#             'property',
#             'ishostkid',
#
#             # # ===== Continuous Variables =====
#             # 'nkill',
#             # 'nwound',
#             # # ===== Categorical variables =====
#             'country_txt',
#                 'region_txt',
#                 'attacktype1_txt',
#                 'targtype1_txt',
#                 'weaptype1_txt',
#
# ]
# target_col = 'success'





''' Feature Representation '''

# select = VarianceThreshold(threshold=(.8 * (1 - .8)))
# feature_selection(select, X_train, y_train,visual=True)
#
# select = RFE(RandomForestClassifier(n_estimators=100, random_state=42),n_features_to_select=40)
# feature_selection(select, X_train, y_train)
#
# select = SelectFromModel(RandomForestClassifier(n_estimators=100, random_state=42),threshold="median")
# feature_selection(select, X_train, y_train,visual=True)

#
# select = ExtraTreesClassifier(n_estimators=20,random_state=0)
# feature_selection2(select, X_train, y_train,visual=True)

#####################################

''' Grid Search '''

# clf = SVC(kernel='rbf')
# param_grid = {'C': [0.001, 0.01, 0.1, 1, 10, 100],
#                'gamma': [0.001, 0.01, 0.05, 0.1, 1, 10, 100]}
#
# grid_clf_acc = GridSearchCV(clf, param_grid = param_grid)
# grid_clf_acc.fit(X_train, y_train)
# y_decision_fn_scores_acc = grid_clf_acc.decision_function(X_test)
#
# print('Grid best parameter (max. accuracy): ', grid_clf_acc.best_params_)
# print('Grid best score (accuracy): ', grid_clf_acc.best_score_)
# #
#
# # alternative metric to optimize over grid parameters: AUC
# grid_clf_auc = GridSearchCV(clf, param_grid = param_grid, scoring = 'roc_auc')
# grid_clf_auc.fit(X_train, y_train)
# y_decision_fn_scores_auc = grid_clf_auc.decision_function(X_test)
#
# print('Test set AUC: ', roc_auc_score(y_test, y_decision_fn_scores_auc))
# print('Grid best parameter (max. AUC): ', grid_clf_auc.best_params_)
# print('Grid best score (AUC): ', grid_clf_auc.best_score_)
# print("Best estimator:\n{}".format(grid_clf_auc.best_estimator_))



############################

lb = LabelEncoder()

data['country_txt'] = lb.fit_transform(data['country_txt'])
data['region_txt'] = lb.fit_transform(data['region_txt'])
data['attacktype1_txt'] = lb.fit_transform(data['attacktype1_txt'])
data['targtype1_txt'] = lb.fit_transform(data['targtype1_txt'])
data['weaptype1_txt'] = lb.fit_transform(data['weaptype1_txt'])


feature_cols = [
            # ===== Spatio-Temporal Variables =====
            'iyear', 'imonth', 'iday',
            'latitude', 'longitude',
            # ===== Binary Variables (1 -> yes or 0 -> no) =====
            'extended',
            'vicinity',
            'doubtterr',
            'multiple',
            # 'success',
            'suicide',
            'claimed',
            'property',
            'ishostkid',

            # # ===== Continuous Variables =====
            # 'nkill',
            # 'nwound',
            # # ===== Categorical variables =====
            'country_txt',
                'region_txt',
                'attacktype1_txt',
                'targtype1_txt',
                'weaptype1_txt',
            ]

target_col = 'success'


data = data[data['iyear']>1999]

''' Train '''

model = DummyClassifier(strategy = 'most_frequent')
train_and_test(model,target_col,feature_cols,data)

# model = LinearRegression()
# train_and_test(model,target_col,feature_cols,data)

model = LogisticRegression()
train_and_test(model,target_col,feature_cols,data)

# model = SVC(kernel='rbf')
# train_and_test(model,target_col,feature_cols,data)

model = SVC(kernel='linear')
train_and_test(model,target_col,feature_cols,data)

model = DecisionTreeClassifier(max_depth=20)
train_and_test(model,target_col,feature_cols,data)

model = RandomForestClassifier()
train_and_test(model,target_col,feature_cols,data)




# print(model.feature_importances_)

# n_features = len(feature_cols)
# plt.barh(range(n_features), model.feature_importances_, align='center')
# # plt.yticks(np.arange(n_features), feature_cols)
# plt.xlabel('Feature Importance')
# plt.ylabel('Feature')
# plt.show()



''' Voting '''

# voting_fet_target(target_col,feature_cols,data)


# X = data[feature_cols].fillna(0)
# y = data[target_col].fillna(0)


# k = 5
# kf = KFold(n_splits=k, random_state=None)
# model = LogisticRegression(solver='liblinear')
#
# acc_score = []
#
# for train_index, test_index in kf.split(X):
#     X_train, X_test = X.iloc[train_index, :], X.iloc[test_index, :]
#     y_train, y_test = y[train_index], y[test_index]
#
#     model.fit(X_train, y_train)
#     pred_values = model.predict(X_test)
#
#     acc = accuracy_score(pred_values, y_test)
#     acc_score.append(acc)
#
# avg_acc_score = sum(acc_score) / k
#
# print('accuracy of each fold - {}'.format(acc_score))
# print('Avg accuracy : {}'.format(avg_acc_score))





# k = 5
# kf = KFold(n_splits=k, random_state=None)
# model = LogisticRegression(solver= 'liblinear')
# result = cross_val_score(model , X, y, cv = kf)
# print("Avg accuracy: {}".format(result.mean()))


# from scipy.stats import sem
# from numpy import mean , std
# from sklearn.datasets import make_classification
#
#
# def evaluate_model(X, y, repeats):
# 	cv = RepeatedKFold(n_splits=10, n_repeats=repeats, random_state=1)
# 	model = LogisticRegression()
# 	scores = cross_val_score(model, X, y, scoring='accuracy', cv=cv, n_jobs=-1)
# 	return scores
#
# repeats = range(1,16)
# results = list()
# for r in repeats:
# 	scores = evaluate_model(X, y, r)
# 	print('>%d mean=%.4f se=%.3f' % (r, mean(scores), sem(scores)))
# 	results.append(scores)
#
# plt.boxplot(results, labels=[str(r) for r in repeats], showmeans=True)
# plt.show()
